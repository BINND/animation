import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import {useSharedValue} from 'react-native-reanimated';
import BackDrop from './BackDrop';
import Tabbar from './Tabbar';
import Icon from 'react-native-vector-icons/Feather';

const Reflectly = props => {
  const open = useSharedValue(0);
  return (
    <View style={styles.container}>
      <BackDrop open={open} />
      <Tabbar open={open} />
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.back}>
        <Icon name="arrow-left" size={30} color={'black'} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingTop: 32,
    alignItems: 'center',
    backgroundColor: '#F5F7FE',
  },
  back: {
    position: 'absolute',
    top: 50,
    left: 10,
  },
});

export default Reflectly;
