import React from 'react';
import {View, Text, StyleSheet, Pressable} from 'react-native';
import Animated, {
  useAnimatedProps,
  useAnimatedStyle,
  useSharedValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated';

const BackDrop = ({open}) => {
  const animatedProps = useAnimatedProps(() => ({
    pointerEvents: open.value < 1 ? 'none' : 'box-none',
  }));
  const style = useAnimatedStyle(() => ({
    backgroundColor: 'grey',
    opacity: 0.6 * open.value,
  }));
  return (
    <Animated.View
      style={[StyleSheet.absoluteFill, style]}
      animatedProps={animatedProps}>
      <Pressable
        style={StyleSheet.absoluteFill}
        onPress={() => (open.value = withTiming(0))}
      />
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {},
});

export default BackDrop;
