import React from 'react';
import { Component } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    AnySizeDragSortableView,
    DragSortableView,
} from 'react-native-drag-sort';

class SortMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fullList:
                this.props.route.params && this.props.route.params.fullList
                    ? this.props.route.params.fullList
                    : [],
        };
        this.sortableViewRef = React.createRef();
    }
    ItemModule = ({ item, index }) => {
        return (
            <View
                style={{
                    marginVertical: 10,
                    backgroundColor: 'white',
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 3,
                    },
                    shadowOpacity: 0.27,
                    shadowRadius: 4.65,

                    marginRight: 15,
                    elevation: 6,
                    borderRadius: 5,
                    marginLeft: 8,
                }}>
                <View
                    style={{
                        position: 'absolute',
                        top: -5,
                        right: -5,
                        backgroundColor: 'green',
                        height: 20,
                        width: 20,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 20,
                        zIndex: 10,
                    }}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: 'white' }}>
                        {index}
                    </Text>
                </View>
                <TouchableOpacity
                    onLongPress={() => {
                        this.sortableViewRef.current?.startTouch(item, index);  //AnySizeDragSortableView use index and item 
                    }}
                    onPressOut={() => {
                        this.sortableViewRef.current?.onPressOut();
                    }}
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingHorizontal: 15,
                        paddingVertical: 10,
                        flexDirection: 'row',
                        overflow: 'visible',
                    }}>
                    <Icon name={item.icon} color={item.color} size={20} />
                    <Text
                        style={{
                            color: '#555555',
                            fontSize: 14,
                            marginTop: 5,
                            lineHeight: 14,
                            marginLeft: 5,
                        }}>
                        {item.name}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };
    render() {
        const { width, height } = Dimensions.get('window');
        const navigation = this.props.navigation;
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name={'chevron-back'} color="#555555" size={30} />
                    </TouchableOpacity>
                    <Text
                        style={{
                            fontWeight: 'bold',
                            fontSize: 18,
                        }}>
                        Sort Module
                    </Text>
                    <TouchableOpacity
                        style={{ borderRadius: 20, overflow: 'hidden' }}
                        onPress={() =>
                            navigation.navigate('Tab', {
                                screen: 'Profile',
                                params: { sortList: this.state.fullList },
                            })
                        }>
                        <Text
                            style={{
                                color: 'white',
                                paddingHorizontal: 10,
                                backgroundColor: '#888888',
                                paddingVertical: 5,
                            }}>
                            Lưu
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, paddingLeft: 15 }}>
                    <AnySizeDragSortableView
                        //AnySizeDragSortableView use index and item 
                        ref={this.sortableViewRef} // vao chinh AnySizeDragSortableView.defaultProps phan bgColor va borderRadius
                        dataSource={this.state.fullList}
                        keyExtractor={(item, index) => item.id} // 1、isRequired
                        renderItem={(item, index) => this.ItemModule({ item, index })}
                        onDataChange={(data, callback) => {
                            this.setState({ fullList: data }, () => {
                                callback(); // isRequired
                            });
                        }}
                    />
                </View>
            </View>
        );
    }
}

export default SortMenu;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
    },
    header: {
        flexDirection: 'row',
        marginHorizontal: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#DDDDDD',
        paddingBottom: 10,
        marginBottom: 20,
    },
});
