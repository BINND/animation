import React, { useState } from 'react';
import {
    Dimensions,
    FlatList,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import { DragSortableView } from 'react-native-drag-sort';
import Toast from 'react-native-tiny-toast';
import Icon from 'react-native-vector-icons/Ionicons';
import ItemSelected from './Item';

const modules = [
    {
        name: 'module1',
    },
    {
        name: 'module2',
    },
    {
        name: 'module3',
    },
    {
        name: 'module4',
    },
    {
        name: 'module5',
    },
];

export default class DragMenu extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            index: -1,
            moduleIndex: 0,
        };
        this.sortableViewRef = React.createRef();
    }

    componentDidMount() {
        this.setState({ list: [...this.props.route.params.list] });
    }

    onViewableItemsChanged = ({ viewableItems, changed }) => {
        // console.log("Visible items are", viewableItems);
        // console.log("Changed in this iteration", changed);
        if (viewableItems.length > 0) {
            this.horizontalList.scrollToIndex({ index: viewableItems[0].index })
            this.setState({ moduleIndex: viewableItems[0].index })
        }
    }

    ItemDefault = (item, index) => {
        let idx = -1;
        idx = this.state.list.findIndex(x => x.id === item.id);
        return (
            <TouchableOpacity
                key={index.toString()}
                onPress={async () => {
                    if (idx < 0) {
                        if (this.state.list.length < 4) {
                            await this.setState({ list: [...this.state.list, item] });
                        } else {
                            Toast.show('Chỉ được chọn tối đa 4 module');
                        }
                    }
                }}
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'white',
                    width: 70,
                    paddingVertical: 10,
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 3,
                    },
                    shadowOpacity: 0.27,
                    shadowRadius: 4.65,
                    marginRight: 15,
                    elevation: 6,
                    marginBottom: 15,
                }}>
                <Icon name={item.icon} color={item.color} size={30} />
                <Text
                    style={{
                        color: '#555555',
                        fontSize: 14,
                        marginTop: 5,
                    }}>
                    {item.name}
                </Text>
                {idx < 0 ? (
                    <View
                        style={{
                            position: 'absolute',
                            backgroundColor: '#999999',
                            borderRadius: 22,
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: 22,
                            height: 22,
                            top: -6,
                            right: -8,
                        }}>
                        <Icon name={'add'} color={'white'} size={18} />
                    </View>
                ) : (
                    <View
                        style={{
                            position: 'absolute',
                            backgroundColor: 'green',
                            borderRadius: 22,
                            justifyContent: 'center',
                            alignItems: 'center',
                            width: 22,
                            height: 22,
                            top: -6,
                            right: -8,
                        }}>
                        <Icon name={'checkmark'} color={'white'} size={18} />
                    </View>
                )}
            </TouchableOpacity>
        );
    };
    render() {
        const { width, height } = Dimensions.get('window');
        const navigation = this.props.navigation;
        const defaultList = [
            {
                name: 'Thêm',
                icon: 'add-circle-outline',
                color: 'red',
                id: 1,
            },
            {
                name: 'Báo thức',
                icon: 'alarm-outline',
                color: 'green',
                id: 2,
            },
            {
                name: 'Albums',
                icon: 'albums-outline',
                color: 'blue',
                id: 3,
            },
            {
                name: 'Nén file',
                icon: 'archive-outline',
                color: 'black',
                id: 4,
            },
            {
                name: 'Biểu đồ',
                icon: 'bar-chart-outline',
                color: 'orange',
                id: 5,
            },
            {
                name: 'Shopping',
                icon: 'basket-outline',
                color: 'pink',
                id: 6,
            },
            {
                name: 'Pin',
                icon: 'battery-full-outline',
                color: 'yellow',
                id: 7,
            },
            {
                name: 'Beer',
                icon: 'beer-outline',
                color: 'cyan',
                id: 8,
            },
            {
                name: 'Bluetooth',
                icon: 'bluetooth-outline',
                color: 'green',
                id: 9,
            },
            {
                name: 'Tool',
                icon: 'briefcase-outline',
                color: 'red',
                id: 10,
            },
        ];
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name={'chevron-back'} color="#555555" size={30} />
                    </TouchableOpacity>
                    <Text
                        style={{
                            fontWeight: 'bold',
                            fontSize: 18,
                        }}>
                        Module Menu
                    </Text>
                    <TouchableOpacity
                        style={{ borderRadius: 20, overflow: 'hidden' }}
                        onPress={() =>
                            navigation.navigate('Tab', {
                                screen: 'Profile',
                                params: { selectedItem: this.state.list },
                            })
                        }>
                        <Text
                            style={{
                                color: 'white',
                                paddingHorizontal: 10,
                                backgroundColor: '#888888',
                                paddingVertical: 5,
                            }}>
                            Lưu
                        </Text>
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        alignItems: 'center',
                        paddingLeft: 10,
                    }}>
                    <DragSortableView
                        ref={this.sortableViewRef}
                        onDataChange={data => {
                            this.setState({ list: data });
                        }}
                        dataSource={this.state.list}
                        parentWidth={90 * 4 + (width - 410)}
                        childrenWidth={85}
                        childrenHeight={80}
                        onDragEnd={props => {
                            this.setState({ index: props });
                        }}
                        keyExtractor={(item, index) => item.id} // required item.id
                        renderItem={(item, index) => {
                            return (
                                <ItemSelected
                                    data={this.state.index}
                                    item={item}
                                    index={index}
                                    onDelete={async () => {
                                        let list = this.state.list;
                                        const idx = list.indexOf(item);
                                        list.splice(idx, 1);
                                        await this.setState({ list: [...list] });
                                    }}
                                    onLongPress={() => {
                                        this.sortableViewRef.current?.startTouch(index); //DragSortableView use only index
                                    }}
                                    onPressOut={() => {
                                        this.sortableViewRef.current?.onPressOut();
                                    }}
                                />
                            );
                        }}
                    />
                </View>
                <Text
                    style={{
                        marginVertical: 20,
                        marginLeft: 15,
                        fontWeight: 'bold',
                        fontSize: 16,
                    }}>
                    Danh sách module
                </Text>
                <View
                    style={{
                        marginBottom: 15,
                    }}>
                    <FlatList
                        decelerationRate="fast"
                        data={modules}
                        ref={ref => {
                            this.horizontalList = ref;
                        }}
                        horizontal showsHorizontalScrollIndicator={false}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity
                                onPress={() => {
                                    this.flatListRef.scrollToIndex({ index });
                                    this.setState({ moduleIndex: index });
                                }}

                                style={[
                                    {
                                        marginLeft: 30,
                                        backgroundColor: '#999999',
                                        paddingVertical: 5,
                                        borderRadius: 15,
                                        paddingHorizontal: 10,
                                    },
                                    this.state.moduleIndex == index
                                        ? { backgroundColor: 'green' }
                                        : null,
                                ]}>
                                <Text
                                    style={{ fontWeight: 'bold', fontSize: 15, color: 'white' }}>
                                    {item.name}
                                </Text>
                            </TouchableOpacity>
                        )}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        decelerationRate="fast"
                        onViewableItemsChanged={this.onViewableItemsChanged}
                        data={modules}
                        ref={ref => {
                            this.flatListRef = ref;
                        }}
                        keyExtractor={(item, index) => index.toString()}
                        viewabilityConfig={{
                            itemVisiblePercentThreshold: 50
                        }}
                        renderItem={({ item, index }) => {
                            return (
                                <View key={index.toString()} style={[index == modules.length - 1 ? { marginBottom: 200 } : null]}>
                                    <Text
                                        style={{
                                            marginVertical: 20,
                                            marginLeft: 15,
                                            fontWeight: 'bold',
                                            fontSize: 16,
                                        }}>
                                        {item.name}
                                    </Text>
                                    <View style={styles.bottomList}>
                                        {defaultList.map((item, index) =>
                                            this.ItemDefault(item, index),
                                        )}
                                    </View>
                                </View>
                            );
                        }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
    },
    header: {
        flexDirection: 'row',
        marginHorizontal: 15,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#DDDDDD',
        paddingBottom: 10,
        marginBottom: 20,
    },
    list: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        marginBottom: 20,
    },
    bottomList: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingLeft: 30,
    },
});
