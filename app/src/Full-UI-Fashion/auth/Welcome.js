import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Button from './Button';

const BR = 75;

const WelCome = props => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.topTitle}>Picture</Text>
      </View>
      <View style={styles.bt}>
        <View style={styles.btContent}>
          <Text style={styles.btTitle}>Let's get started</Text>
          <Text style={styles.btText}>
            Login to your account below or signup for an amazing experience
          </Text>
          <Button
            label={'Have an account? Login'}
            style={styles.button}
            variant={'last'}
            onPress={() => {
              props.navigation.navigate('Login');
            }}
          />
          <Button label={`Join us, it's free`} style={styles.button} />
          <Button label={'Forgot password?'} style={styles.button} noBg />
        </View>
      </View>
    </View>
  );
};

export default WelCome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  top: {
    width: '100%',
    height: '50%',
    backgroundColor: 'rgba(12,13,52,0.2)',
    borderBottomRightRadius: BR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bt: {
    width: '100%',
    height: '50%',
    backgroundColor: 'rgba(12,13,52,0.2)',
  },
  btContent: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: BR,
    zIndex: 10,
    alignItems: 'center',
  },
  topTitle: {
    fontSize: 50,
    color: 'white',
    fontFamily: 'SFProText-Bold',
  },
  btTitle: {
    fontSize: 24,
    fontFamily: 'SFProText-Semibold',
    textAlign: 'center',
    lineHeight: 30,
    marginBottom: 12,
    marginTop: 40,
  },
  btText: {
    fontSize: 16,
    fontFamily: 'SFProText-Regular',
    lineHeight: 24,
    color: '#0C0D34',
    textAlign: 'center',
    marginBottom: 15,
  },
  button: {marginVertical: 12},
});
