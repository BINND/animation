import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Story = ({x}) => {
  console.log('X', x);
  return (
    <View style={styles.container}>
      <Text>Story</Text>
    </View>
  );
};

export default Story;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
