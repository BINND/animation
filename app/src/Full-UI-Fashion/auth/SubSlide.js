import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import Button from './Button';
import {SILDER_HEIGHT} from './OnBoard';
const {width} = Dimensions.get('window');

const SubSlide = ({title, content, last, onPress}) => {
  return (
    <View style={styles.container}>
      <View style={[styles.tileContainer]}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.content}>{content}</Text>
        <Button
          label={last ? "Let's get started" : 'Next'}
          variant={last ? 'last' : 'default'}
          onPress={onPress}
        />
      </View>
    </View>
  );
};

export default SubSlide;

const styles = StyleSheet.create({
  container: {
    width: width,
    padding: 40,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontFamily: 'SFProText-Semibold',
    textAlign: 'center',
    lineHeight: 30,
    marginBottom: 12,
  },
  content: {
    fontSize: 16,
    fontFamily: 'SFProText-Regular',
    lineHeight: 24,
    color: '#0C0D34',
    textAlign: 'center',
    marginBottom: 40,
  },
  tileContainer: {
    height: 100,
    justifyContent: 'center',
    color: '#0C0D34',
    alignItems: 'center',
  },
});
