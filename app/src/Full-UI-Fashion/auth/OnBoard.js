import React from 'react';
import {useRef, useEffect, useState} from 'react';
import {View, Text, StyleSheet, ScrollView, Dimensions} from 'react-native';
import Animated, {
  interpolateColor,
  multiply,
  useAnimatedRef,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
  useValue,
  withSpring,
  withTiming,
} from 'react-native-reanimated';
import Slide from './Slide';
import SubSlide from './SubSlide';
import Dot from './DotSlide';
import {divide} from 'lodash';
import DotSlide from './DotSlide';

const {width, height} = Dimensions.get('window');

const BR = 75;

const slides = [
  {
    label: 'Relaxed',
    color: '#BFEAF5',
    title: 'Find Your Outfits',
    content:
      "Confused about your outfit? Don't worry!\nFind the best outfit there!",
    picture: require('../../../assets/fashion/1.jpg'),
  },
  {
    label: 'Playful',
    color: '#BEECC4',
    title: 'Hear It First, Wear It First',
    content:
      'Hating the clothes in your wardrobe?\nExplore hundreds of outfit ideas',
    picture: require('../../../assets/fashion/1.jpg'),
  },
  {
    label: 'Excentric',
    color: '#FFE4D9',
    title: 'Your Style, Your Way',
    content: 'Create your individual & unique style and look amazing everyday',
    picture: require('../../../assets/fashion/1.jpg'),
  },
  {
    label: 'Funky',
    color: '#FFDDDD',
    title: 'Look Good, Feel Good',
    content:
      'Discover the latest trends in fashion and explore your personality',
    picture: require('../../../assets/fashion/1.jpg'),
  },
];

export const SILDER_HEIGHT = 0.61 * height;

const OnBoard = props => {
  const scroll = useRef();
  const x = useSharedValue(0);

  const onScroll = useAnimatedScrollHandler({
    onScroll: ({contentOffset: {x: value}}) => {
      x.value = value;
    },
  });

  const style = useAnimatedStyle(() => {
    return {
      backgroundColor: interpolateColor(
        x.value,
        slides.map((_, index) => index * width),
        slides.map(item => item.color),
      ),
    };
  });
  const bt = useAnimatedStyle(() => {
    return {
      backgroundColor: interpolateColor(
        x.value,
        slides.map((_, index) => index * width),
        slides.map(item => item.color),
      ),
    };
  });

  const tf = useAnimatedStyle(() => {
    return {
      transform: [{translateX: x.value * -1}],
    };
  });

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.slider, style]}>
        <Animated.ScrollView
          ref={scroll}
          {...{onScroll}}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}
          bounces={false}
          horizontal
          snapToInterval={width}
          decelerationRate="fast">
          {slides.map(({label, picture}, index) => (
            <Slide right={!!(index % 2)} key={index} {...{label, picture}} />
          ))}
        </Animated.ScrollView>
      </Animated.View>
      <View style={styles.footer}>
        <Animated.View style={[{...StyleSheet.absoluteFill}, bt]} />
        <View style={styles.footerContainer}>
          <View style={styles.pagination}>
            {slides.map((_, index) => (
              <DotSlide key={index} {...{index}} x={x} width={width} />
            ))}
          </View>
          <Animated.View
            style={[
              {flex: 1, flexDirection: 'row', width: width * slides.length},
              tf,
            ]}>
            {slides.map(({title, content}, index) => (
              <SubSlide
                last={index == slides.length - 1}
                key={index}
                {...{title, content}}
                onPress={() => {
                  if (index == slides.length - 1) {
                    props.navigation.navigate('Welcome');
                  } else {
                    if (scroll.current) {
                      scroll.current.scrollTo({
                        x: width * (index + 1),
                        animated: true,
                      });
                    }
                  }
                }}
              />
            ))}
          </Animated.View>
        </View>
      </View>
    </View>
  );
};

export default OnBoard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  slider: {
    height: SILDER_HEIGHT,
    borderBottomRightRadius: BR,
  },
  footer: {
    flex: 1,
  },
  footerContainer: {
    flex: 1,
    backgroundColor: 'white',
    borderTopLeftRadius: BR,
  },
  pagination: {
    height: BR,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    ...StyleSheet.absoluteFill,
  },
});
