import React from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import {SILDER_HEIGHT} from './OnBoard';
const {width} = Dimensions.get('window');

const Slide = ({label, right, picture}) => {
  const transform = [
    {translateY: (SILDER_HEIGHT - 100) / 2},
    {translateX: right ? width / 2 - 50 : -width / 2 + 50},
    {rotate: right ? '-90deg' : '90deg'},
  ];
  return (
    <View style={styles.container}>
      <View style={[styles.tileContainer, {transform}]}>
        <Text style={styles.title}>{label}</Text>
      </View>
    </View>
  );
};

export default Slide;

const styles = StyleSheet.create({
  container: {
    width: width,
  },
  title: {
    fontSize: 80,
    fontFamily: 'SFProText-Bold',
    color: 'white',
    textAlign: 'center',
    lineHeight: 80,
  },
  tileContainer: {
    height: 100,
    justifyContent: 'center',
  },
});
