import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {RectButton} from 'react-native-gesture-handler';

const Button = ({label, variant, onPress, style, noBg}) => {
  const backgroundColor = variant == 'last' ? '#2CB9B0' : 'rgba(12,13,52,0.05)';
  const color = variant == 'last' ? 'white' : '#0C0D34';
  return (
    <RectButton
      style={[
        styles.container,
        noBg ? null : {backgroundColor: backgroundColor},
        style,
      ]}
      onPress={onPress}>
      <Text style={[styles.label, {color: color}]}>{label}</Text>
    </RectButton>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    borderRadius: 25,
    height: 50,
    width: 245,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontFamily: 'SFProText-Semibold',
    fontSize: 15,
    textAlign: 'center',
  },
});
