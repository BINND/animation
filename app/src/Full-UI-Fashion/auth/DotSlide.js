import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useDerivedValue,
} from 'react-native-reanimated';

const DotSlide = ({index, x, width}) => {
  const newVal = useDerivedValue(() => {
    return x.value / width;
  });
  const animated = useAnimatedStyle(() => {
    const opacity = interpolate(
      newVal.value,
      [index - 1, index, index + 1],
      [0.5, 1, 0.5],
      Extrapolate.CLAMP,
    );

    const scale = interpolate(
      newVal.value,
      [index - 1, index, index + 1],
      [1, 1.25, 1],
      Extrapolate.CLAMP,
    );
    return {
      opacity,
      transform: [{scale}],
    };
  });

  return <Animated.View style={[styles.container, animated]}></Animated.View>;
};

export default DotSlide;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2CB9B0',
    width: 8,
    height: 8,
    borderRadius: 4,
    margin: 4,
  },
});
