import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import CheckBox from '../../Components/CheckBox';
import Container from '../../Components/Container';
import CustomeInput from '../../Components/CustomInput';
import SocialLogin from '../../Components/SocialLogin';
import {appColors} from '../../constans/colors';
import Button from '../auth/Button';

class Login extends Component {
  state = {
    userName: '',
    validUserName: false,
    passWord: '',
    validPassword: false,
    checkBox: false,
  };
  checkValidEmail = () => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(this.state.userName).toLowerCase());
  };
  render() {
    const footer = (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <SocialLogin />
        <View style={{flexDirection: 'row'}}>
          <Text style={{color: 'white'}}>Don't have an account? </Text>
          <TouchableOpacity>
            <Text style={{color: '#2CB9B0'}}>Sign Up here</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
    return (
      <Container {...{footer}}>
        <Text
          style={{
            fontSize: 28,
            fontFamily: 'SFProText-Semibold',
            textAlign: 'center',
            marginTop: 40,
          }}>
          Welcome back
        </Text>
        <Text
          style={{
            marginTop: 20,
            fontSize: 16,
            fontFamily: 'SFProText-Regular',
            color: appColors.text,
            textAlign: 'center',
          }}>
          Use your credentials below and login to your acount
        </Text>
        <CustomeInput
          value={this.state.userName}
          onChangeText={text => {
            this.setState({
              userName: text,
              validUserName: this.checkValidEmail(),
            });
          }}
          placeholder={'Enter your Email'}
          valid={this.state.validUserName}
          icon={'mail'}
        />
        <CustomeInput
          value={this.state.passWord}
          onChangeText={text => {
            this.setState({
              passWord: text,
              validPassword: text == '' ? false : true,
            });
          }}
          placeholder={'Enter your Password'}
          valid={this.state.validPassword}
          icon={'lock'}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '80%',
            marginTop: 20,
            alignSelf: 'center',
            alignItems: 'center',
          }}>
          <CheckBox
            selectedValue={true}
            value={this.state.checkBox}
            title={'Remember me'}
            onPress={() => this.setState({checkBox: !this.state.checkBox})}
          />
          <TouchableOpacity>
            <Text style={{color: appColors.primary}}>Forgot password</Text>
          </TouchableOpacity>
        </View>
        <Button
          label={'Login to your account'}
          variant="last"
          style={{
            alignSelf: 'center',
            marginTop: 70,
          }}
        />
      </Container>
    );
  }
}
export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
