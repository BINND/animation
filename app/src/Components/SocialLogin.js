import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const SocialLogin = props => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.btContainer}>
        <Icon name={'facebook'} color={'#3C5A99'} size={25} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.btContainer}>
        <Icon name={'google'} color={'#EA4355'} size={25} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.btContainer}>
        <Icon name={'apple'} color={'#000'} size={25} />
      </TouchableOpacity>
    </View>
  );
};

export default SocialLogin;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: 160,
    justifyContent: 'space-between',
    marginBottom: 40,
  },
  btContainer: {
    backgroundColor: 'white',
    height: 45,
    width: 45,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
