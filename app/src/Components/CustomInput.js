import React from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {appColors} from '../constans/colors';

const CustomeInput = ({
  value,
  onChangeText,
  placeholder,
  valid,

  icon,
}) => {
  return (
    <View
      style={[
        styles.container,
        value != ''
          ? {borderColor: valid ? appColors.primary : appColors.danger}
          : null,
      ]}>
      <Icon
        name={icon}
        size={20}
        style={styles.icon}
        color={
          value != ''
            ? valid
              ? appColors.primary
              : appColors.danger
            : appColors.text
        }
      />
      <TextInput
        value={value}
        onChangeText={onChangeText}
        style={[
          styles.input,
          value != ''
            ? {color: valid ? appColors.primary : appColors.danger}
            : null,
        ]}
        placeholder={placeholder}
        placeholderTextColor={appColors.text}
      />
      {value != '' ? (
        <View
          style={{
            backgroundColor: valid ? appColors.primary : appColors.danger,
            borderRadius: 25,
            padding: 3,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name={valid ? 'check' : 'x'}
            size={15}
            style={styles.validIcon}
            color="white"
          />
        </View>
      ) : null}
    </View>
  );
};

export default CustomeInput;

const styles = StyleSheet.create({
  container: {
    width: '80%',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 20,
    borderRadius: 4,
    borderWidth: 1,
    alignSelf: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderColor: appColors.text,
  },
  icon: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    color: appColors.subTitle,
  },
  validIcon: {},
});
