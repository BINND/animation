import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  StatusBar,
} from 'react-native';

const {width} = Dimensions.get('window');
const ratio = 750 / 1125;
const height = width * ratio;

const Container = props => {
  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" />
      <View style={{backgroundColor: 'white'}}>
        <View
          style={{
            overflow: 'hidden',
            height: height * 0.61,
            borderBottomLeftRadius: 75,
          }}>
          <Image
            source={require('../../assets/fashion/bg.jpeg')}
            style={styles.img}
            height={height}
            width={width}
          />
        </View>
      </View>
      <View
        style={{
          overflow: 'hidden',
          height: height * 0.61,
        }}>
        <Image
          source={require('../../assets/fashion/bg.jpeg')}
          style={{
            ...StyleSheet.absoluteFillObject,
            height: height,
            width: width,
            top: -height * 0.61,
          }}
          height={height}
          width={width}
        />
        <View
          style={{
            ...StyleSheet.absoluteFill,
            backgroundColor: 'white',
            borderTopRightRadius: 75,
          }}
        />
      </View>
      <View
        style={{
          flex: 1,
          backgroundColor: 'white',
          borderBottomLeftRadius: 75,
          borderBottomRightRadius: 75,
        }}>
        <View
          style={{
            height: 569,
            borderRadius: 75,
            top: -height * 0.61,
          }}>
          {props.children}
        </View>
      </View>
      <View
        style={{
          backgroundColor: '#0C0D34',
          justifyContent: 'center',
          alignItems: 'center',
          paddingBottom: 60,
          paddingTop: 40,
        }}>
        {props.footer}
      </View>
    </View>
  );
};

export default Container;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0C0D34',
  },
  img: {},
});
