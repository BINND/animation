import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {appColors} from '../constans/colors';
import {appFont} from '../constans/fonts';

const CheckBox = props => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <View
        style={{
          backgroundColor:
            props.value == props.selectedValue ? appColors.primary : 'white',
          borderRadius: 2,
          borderWidth: 1,
          borderColor: appColors.primary,
          padding: 2,
          marginRight: 10,
        }}>
        <Icon name={'check'} size={15} color={'white'} />
      </View>
      <Text
        style={{
          fontFamily: appFont.SFProTextRegular,
          color: appColors.subTitle,
        }}>
        {props.title}
      </Text>
    </TouchableOpacity>
  );
};

export default CheckBox;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
