import React, { Component, useRef, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withSpring,
  withTiming,
  withDecay,
  withDelay,
  withRepeat,
  withSequence,
  useAnimatedGestureHandler,
} from 'react-native-reanimated';
import {
  TapGestureHandler,
  PanGestureHandler,
} from 'react-native-gesture-handler';

Home = () => {
  const [positon, setPositon] = useState({});
  const pressed = useSharedValue(false);
  const startingPosition = 100;
  const x = useSharedValue(startingPosition);
  const y = useSharedValue(startingPosition);

  const cancel = useSharedValue(200);

  const cancelStyle = useAnimatedStyle(() => {
    return {
      transform: [{ translateY: cancel.value }],
    };
  });

  const eventHandler = useAnimatedGestureHandler({
    onStart: (event, ctx) => {
      pressed.value = true;
      ctx.startX = x.value;
      ctx.startY = y.value;
      cancel.value = withSpring(0);
    },
    onActive: (event, ctx) => {
      x.value = withSpring(ctx.startX + event.translationX);
      y.value = withSpring(ctx.startY + event.translationY);
    },
    onEnd: (event, ctx) => {
      pressed.value = false;
      cancel.value = withSpring(200);
      if (
        event.absoluteX > 150 &&
        event.absoluteX < 250 &&
        event.absoluteY > 650 &&
        event.absoluteY < 750
      ) {
        pressed.value = false;
        x.value = withSpring(150);
        y.value = withSpring(850);
        cancel.value = withSpring(200);
      }
    },
  });

  const uas = useAnimatedStyle(() => {
    return {
      backgroundColor: pressed.value ? '#FEEF86' : '#001972',
      transform: [{ translateX: x.value }, { translateY: y.value }],
    };
  });

  const cancelView = useRef();

  return (
    <View style={styles.container}>
      <PanGestureHandler onGestureEvent={eventHandler}>
        <Animated.View
          style={[
            {
              borderRadius: 100,
              height: 100,
              width: 100,
              backgroundColor: '#001972',
            },
            uas,
          ]}
        />
      </PanGestureHandler>
      <TouchableOpacity
        style={{
          position: 'absolute',
          right: 50,
          top: 100,
        }}
        onPress={() => {
          x.value = withSpring(startingPosition);
          y.value = withSpring(startingPosition);
        }}>
        <Text style={{ color: 'green' }}>Lên</Text>
      </TouchableOpacity>
      <Animated.View
        onLayout={event => {
          const layout = event.nativeEvent.layout;
          const positon = {
            x: layout.x,
            y: layout.y,
          };
          setPositon(positon);
        }}
        ref={cancelView}
        style={[
          {
            borderRadius: 100,
            borderWidth: 1,
            borderColor: 'black',
            height: 100,
            width: 100,
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            bottom: 100,
            right: (Dimensions.get('window').width - 100) / 2,
          },
          cancelStyle,
        ]}>
        <Text style={{ fontSize: 35 }}>X</Text>
      </Animated.View>
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
