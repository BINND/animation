import React, {useState, useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Platform,
  Dimensions,
  StatusBar,
} from 'react-native';
import Animated, {
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
  interpolate,
  withSpring,
  Transition,
  Transitioning,
  Easing,
} from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {
  Directions,
  FlingGestureHandler,
  State,
} from 'react-native-gesture-handler';
import posed, {Transition as PoseTransition} from 'react-native-pose';

const data = [
  {
    image: require('../assets/1.jpeg'),
    name: 'Cái Bánh Thứ 1',
    detail:
      'Đây là 1 cái bánh tải từ trên mạng ko biết ngon ko nhưng nhìn ngon đấy. Với nguyên vật liệu bí ẩn vì tải trên mạng nhưng nhìn ngon là được.',
    hot: '1 hour',
    medal: 10,
    electric: 'easy',
    chemastry: 'Vegan',
    water: 'dehydrated',
  },
  {
    image: require('../assets/2.jpeg'),
    name: 'Cái Bánh Thứ 2',
    detail:
      'Cái bánh được dành riêng cho thằng Đông với đầu tóc mì tôm nhưng màu ko phải màu mì tôm mà màu mì cua, được chia năm xẻ bảy bởi những con người rất tuyệt vời',
    hot: '2 hour',
    medal: 20,
    electric: 'normal',
    chemastry: 'Chemystry',
    water: 'water',
  },
  {
    image: require('../assets/3.jpeg'),
    name: 'Cái Bánh Thứ 3',
    detail:
      'Đây là 1 cái bánh tải từ trên mạng ko biết ngon ko nhưng nhìn ngon đấy. Với nguyên vật liệu bí ẩn vì tải trên mạng nhưng nhìn ngon là được.',
    hot: '1 hour',
    medal: 10,
    electric: 'easy',
    chemastry: 'Vegan',
    water: 'dehydrated',
  },
  {
    image: require('../assets/4.jpeg'),
    name: 'Cái Bánh Thứ 4',
    detail:
      'Cái bánh được dành riêng cho thằng Đông với đầu tóc mì tôm nhưng màu ko phải màu mì tôm mà màu mì cua, được chia năm xẻ bảy bởi những con người rất tuyệt vời',
    hot: '2 hour',
    medal: 20,
    electric: 'normal',
    chemastry: 'Chemystry',
    water: 'water',
  },
];

const height = Dimensions.get('window').height;
const transition = (
  <Transition.Together>
    <Transition.Out
      type="slide-left"
      interpolation="easeIn"
      durationMs={1000}
    />
    <Transition.Change />
    <Transition.In
      type="slide-bottom"
      interpolation="easeOut"
      durationMs={1000}
    />
  </Transition.Together>
);

const congig = {
  transition: {
    type: 'tween',
    easing: Easing.elastic(0.9),
    duration: 500,
  },
};

const PoseView = posed.View({
  enter: {opacity: 1, rotate: '0deg', ...congig},
  exit: {opacity: 0, rotate: '180deg', ...congig},
});

const Chat = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  const activeIndex = React.useRef(useSharedValue(0)).current;
  const animation = React.useRef(useSharedValue(0)).current;

  const astyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: withSpring(
            interpolate(animation.value, [-1, 0, 1], [height, 0, -height]),
          ),
        },
      ],
    };
  });

  const setActiveIndex = useCallback(index => {
    animation.value = index;
    activeIndex.value = index;
    setCurrentIndex(index);
  });

  const darkStyle = currentIndex % 2 != 0 ? {color: 'white'} : null;
  const ref = React.useRef();

  React.useEffect(() => {
    StatusBar.setBarStyle(
      currentIndex % 2 == 0 ? 'dark-content' : 'light-content',
      true,
    );
  });

  return (
    <FlingGestureHandler
      key={'down'}
      direction={Directions.DOWN}
      onHandlerStateChange={ev => {
        if (ev.nativeEvent.state == State.END) {
          if (currentIndex == 0) {
            setActiveIndex(3);
          } else {
            setActiveIndex(currentIndex - 1);
          }
        }
      }}>
      <FlingGestureHandler
        key={'up'}
        direction={Directions.UP}
        onHandlerStateChange={ev => {
          if (ev.nativeEvent.state == State.END) {
            if (currentIndex == data.length - 1) {
              setActiveIndex(0);
            } else {
              setActiveIndex(currentIndex + 1);
            }
          }
        }}>
        <View style={styles.container}>
          <StatusBar translucent backgroundColor="transparent" />
          <View
            style={{
              height: height,
              width: '100%',
              justifyContent: 'center',
              alignContent: 'center',
              zIndex: 100,
            }}>
            <View style={styles.name}>
              <Text
                style={[
                  {
                    color: '#282546',
                    fontSize: 30,
                    fontWeight: 'bold',
                    left: 10,
                  },
                  darkStyle,
                ]}>
                {data[currentIndex].name}
              </Text>
            </View>
            <View
              style={{
                position: 'absolute',
                left: 10,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <Icon
                  name="fire"
                  color={'#B5B7B9'}
                  size={30}
                  style={darkStyle}
                />
                <Text
                  style={[
                    {
                      color: '#B5B7B9',
                      fontWeight: '700',
                      fontSize: 16,
                      marginLeft: 10,
                    },
                    darkStyle,
                  ]}>
                  {data[currentIndex].hot}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <Icon
                  name="badge"
                  color={'#B5B7B9'}
                  size={30}
                  style={darkStyle}
                />
                <Text
                  style={[
                    {
                      color: '#B5B7B9',
                      fontWeight: '700',
                      fontSize: 16,
                      marginLeft: 10,
                    },
                    darkStyle,
                  ]}>
                  {data[currentIndex].medal}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <Icon
                  name="energy"
                  color={'#B5B7B9'}
                  size={30}
                  style={darkStyle}
                />
                <Text
                  style={[
                    {
                      color: '#B5B7B9',
                      fontWeight: '700',
                      fontSize: 16,
                      marginLeft: 10,
                    },
                    darkStyle,
                  ]}>
                  {data[currentIndex].electric}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <Icon
                  name="chemistry"
                  color={'#B5B7B9'}
                  size={30}
                  style={darkStyle}
                />
                <Text
                  style={[
                    {
                      color: '#B5B7B9',
                      fontWeight: '700',
                      fontSize: 16,
                      marginLeft: 10,
                    },
                    darkStyle,
                  ]}>
                  {data[currentIndex].chemastry}
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginBottom: 20,
                }}>
                <Icon
                  name="drop"
                  color={'#B5B7B9'}
                  size={30}
                  style={darkStyle}
                />
                <Text
                  style={[
                    {
                      color: '#B5B7B9',
                      fontWeight: '700',
                      fontSize: 16,
                      marginLeft: 10,
                    },
                    darkStyle,
                  ]}>
                  {data[currentIndex].water}
                </Text>
              </View>
            </View>
            <PoseTransition>
              {currentIndex % 2 == 0 ? (
                <PoseView
                  key="img"
                  style={[
                    styles.imgContainer,
                    currentIndex % 2 != 0 ? {borderColor: 'white'} : null,
                  ]}>
                  <View
                    style={{
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 4,
                      },
                      shadowOpacity: 0.5,
                      shadowRadius: 5.46,

                      elevation: 9,
                    }}>
                    <Image
                      source={data[currentIndex].image}
                      style={styles.img}
                    />
                  </View>
                </PoseView>
              ) : (
                <PoseView
                  key="img2"
                  style={[
                    styles.imgContainer,
                    currentIndex % 2 != 0 ? {borderColor: 'white'} : null,
                  ]}>
                  <View
                    style={{
                      shadowColor: '#000',
                      shadowOffset: {
                        width: 0,
                        height: 4,
                      },
                      shadowOpacity: 0.5,
                      shadowRadius: 5.46,

                      elevation: 9,
                    }}>
                    <Image
                      source={data[currentIndex].image}
                      style={styles.img}
                    />
                  </View>
                </PoseView>
              )}
            </PoseTransition>
            <View
              style={{
                position: 'absolute',
                bottom: Platform.OS === 'android' ? 100 : 150,
              }}>
              <Text
                style={[
                  {
                    paddingHorizontal: 12,
                    color: '#282546',
                    fontWeight: '400',
                  },
                  darkStyle,
                ]}>
                {data[currentIndex].detail}
              </Text>
            </View>
          </View>
          <Animated.View
            style={[
              StyleSheet.absoluteFillObject,
              {height: height * data.length},
              astyle,
            ]}>
            {data.map((_, i) => {
              return (
                <View
                  key={i}
                  style={{
                    height,
                    backgroundColor: i % 2 == 0 ? 'transparent' : '#282546',
                  }}></View>
              );
            })}
          </Animated.View>
        </View>
      </FlingGestureHandler>
    </FlingGestureHandler>
  );
};

export default Chat;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    height: 280,
    width: 280,
    borderRadius: 250,
  },
  imgContainer: {
    borderLeftWidth: 1.5,
    borderRightWidth: 1,
    borderBottomWidth: 0,
    borderTopWidth: 0,
    padding: 20,
    borderRadius: 200,
    position: 'absolute',
    right: -140,
    borderColor: '#282546',
  },
  name: {
    position: 'absolute',
    top: Platform.OS === 'android' ? 100 : 170,
  },
});
