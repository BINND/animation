export const appColors = {
  primary: '#2CB9B0',
  subTitle: '#0C0D34',
  text: 'rgba(12,13, 52,0.7)',
  gray: '#F4F0EF',
  darkGray: '#8A8D90',
  danger: '#FF0058',
};
