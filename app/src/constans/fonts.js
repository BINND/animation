export const appFont = {
  SFProTextSemibold: 'SFProText-Semibold',
  SFProTextBold: 'SFProText-Bold',
  SFProTextRegular: 'SFProText-Regular',
};
