import React, {useState, useEffect, Component} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text as RNText,
  Platform,
} from 'react-native';
import {PanGestureHandler} from 'react-native-gesture-handler';
import {snap} from '@popmotion/popcorn';
import Svg, {Path, G, Text, TSpan} from 'react-native-svg';
import * as d3Shape from 'd3-shape';
import color from 'randomcolor';
import Animated, {
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withDecay,
  interpolate,
  Extrapolate,
  withTiming,
  runOnJS,
  call,
  useCode,
  useDerivedValue,
} from 'react-native-reanimated';
import {setState} from 'expect';

const numberofSegments = 7;
const fontSize = 26;
const oneTurn = 360;
const angleBySebment = oneTurn / numberofSegments;
const angleOffset = angleBySebment / 2;

const {width, heihgt} = Dimensions.get('window');

const wheelSize = width * 0.9;
const makeWheel = () => {
  const data = Array.from({length: numberofSegments}).fill(1);
  const arcs = d3Shape.pie()(data);
  const colors = color({
    luminosity: 'dark',
    count: numberofSegments,
  });
  arcs.sort(function (a, b) {
    return a.index - b.index;
  });
  return arcs.map((arc, index) => {
    console.log(
      Platform.OS === 'android' ? `and-${index}` : `ios-${index}`,
      arc,
    );
    const instance = d3Shape
      .arc()
      .padAngle(0.01)
      .outerRadius(width / 2)
      .innerRadius(20);
    return {
      path: instance(arc),
      color: colors[index],
      value: Math.round(Math.random() * 10 + 200),
      centroid: instance.centroid(arc),
    };
  });
};

class Wheel extends Component {
  render() {
    return <View style={styles.container}>{this.renderWheel()}</View>;
  }

  _wheelPath = makeWheel();

  renderWheel = () => {
    return (
      <View>
        <Svg
          height={wheelSize}
          width={wheelSize}
          viewBox={`0 0 ${width} ${width}`}>
          <G x={width / 2} y={width / 2}>
            {this._wheelPath.map((arc, i) => {
              const [x, y] = arc.centroid;
              const number = arc.value.toString();

              return (
                <G key={`arc-${i}`}>
                  <Path d={arc.path} fill={arc.color} />
                  <G
                    originX={x}
                    originY={y}
                    rotation={(i * oneTurn) / numberofSegments + angleOffset}>
                    <Text
                      x={x}
                      y={y}
                      fill={'white'}
                      textAnchor="middle"
                      fontSize={fontSize}>
                      {Array.from({length: number.length}).map((_, j) => {
                        return (
                          <TSpan
                            x={x}
                            dy={fontSize}
                            key={`arc-${i}-slide-${j}`}>
                            {number.charAt(j)}
                          </TSpan>
                        );
                      })}
                    </Text>
                  </G>
                </G>
              );
            })}
          </G>
        </Svg>
      </View>
    );
  };
}

export default Wheel;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
