import React, {Component} from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity} from 'react-native';
import WheelOfFortune from '../Components/WheelOfFortune';

class Wheel2 extends Component {
  render() {
    const wheelOptions = {
      rewards: [
        '%20',
        '%30',
        '%40',
        '%50',
        '%60',
        'FREE',
        '%20',
        '%30',
        '%40',
        '%50',
        '%60',
        'FREE',
      ],
      knobSize: 40,
      borderWidth: 5,
      borderColor: '#000',
      innerRadius: 30,
      winner: 1,
      duration: 10000,
      backgroundColor: 'transparent',
      textAngle: 'horizontal',
      knobSource: require('../../assets/arrow_down.png'),
      knobSize: 30,
      getWinner: (value, index) => {
        this.setState({winnerValue: value, winnerIndex: index});
      },
      onRef: ref => (this.child = ref),
    };
    return (
      <View style={styles.container}>
        <WheelOfFortune options={wheelOptions} />
        <TouchableOpacity
          onPress={() => {
            this.child._onPress();
          }}
          style={{
            backgroundColor: '#D82B2B',
            width: 100,
            height: 40,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 4,
          }}>
          <Text style={{color: 'white', fontWeight: 'bold'}}>Quay</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default Wheel2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
