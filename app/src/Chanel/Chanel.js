import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Item, {MAX_HEIGHT} from './Item';
import Icon from 'react-native-vector-icons/Feather';
import Animated, {
  useAnimatedScrollHandler,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';

const Chanel = props => {
  const y = useSharedValue(0);
  const onScroll = useAnimatedScrollHandler({
    onScroll: ({contentOffset: {y: value}}) => {
      y.value = withSpring(value);
    },
  });
  return (
    <>
      <StatusBar hidden />
      <Animated.ScrollView
        decelerationRate="fast"
        snapToInterval={MAX_HEIGHT}
        contentContainerStyle={{
          height: (items.length + 1) * MAX_HEIGHT,
        }}
        scrollEventThrottle={16}
        onScroll={onScroll}
        style={styles.scrollView}>
        {items.map((item, index) => (
          <Item item={item} index={index} key={index} y={y} />
        ))}
      </Animated.ScrollView>
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.back}>
        <Icon name="arrow-left" size={30} color={'white'} />
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  scrollView: {
    backgroundColor: 'black',
  },
  back: {
    position: 'absolute',
    top: 50,
    left: 10,
  },
});

export default Chanel;

const items = [
  {
    title: 'Upcoming Show Live from Paris',
    subtitle: 'SPRING-SUMMER 2021',
    picture: require('../../assets/Chanel/chanel.jpg'),
    top: 0,
  },
  {
    title: 'In Boutiques',
    subtitle: 'FALL-WINTER 2020/21',
    picture: require('../../assets/Chanel/sonnie-hiles-pU4J5VFnqCQ-unsplash-with-gradient.jpg'),
    top: 0,
  },
  {
    title: 'Deauville Film Festival',
    subtitle: 'CHANEL IN CINEMA',
    picture: require('../../assets/Chanel/laura-chouette-NFrPPyGe5q0-unsplash-with-gradient.jpg'),
    top: 0,
  },
  {
    title: 'IN BOUTIQUES',
    subtitle: "Métiers d'art 2019/20",
    picture: require('../../assets/Chanel/butsarakham-buranaworachot-au6Gddf1pZQ-unsplash.jpg'),
    top: 0,
  },
  {
    title: 'Haute Couture',
    subtitle: 'FALL-WINTER 2020/21',
    picture: require('../../assets/Chanel/khaled-ghareeb-upepKTbwm3A-unsplash.jpg'),
    top: 50,
  },
  {
    title: 'Balade en Méditerranée',
    subtitle: 'CRUISE 2020/21',
    picture: require('../../assets/Chanel/christopher-campbell-A3QXXEfcA1U-unsplash.jpg'),
    top: 0,
  },
  {
    title: 'Spring-Summer 2020 Campaign',
    subtitle: 'EYEWEAR',
    picture: require('../../assets/Chanel/chase-fade-Pb13EUxzMDw-unsplash.jpg'),
    top: 0,
  },
];
