import React from 'react';
import {Dimensions, StyleSheet, View, Image, Text} from 'react-native';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';

const Item = props => {
  const container = useAnimatedStyle(() => ({
    height: interpolate(
      props.y.value,
      [(props.index - 1) * MAX_HEIGHT, props.index * MAX_HEIGHT],
      [MIN_HEIGHT, MAX_HEIGHT],
      Extrapolate.CLAMP,
    ),
  }));
  const titleStyle = useAnimatedStyle(() => ({
    opacity: interpolate(
      props.y.value,
      [(props.index - 1) * MAX_HEIGHT, props.index * MAX_HEIGHT],
      [0, 1],
      Extrapolate.CLAMP,
    ),
  }));
  return (
    <Animated.View style={[styles.container, container]}>
      <Image
        source={props.item.picture}
        style={[styles.picture]}
        resizeMode="cover"
      />
      <View style={styles.titleContainer}>
        <Text style={styles.subtitle}>{props.item.subtitle.toUpperCase()}</Text>
        <View style={styles.mainTitle}>
          <Animated.View style={titleStyle}>
            <Text style={styles.title}>{props.item.title.toUpperCase()}</Text>
          </Animated.View>
        </View>
      </View>
    </Animated.View>
  );
};

const {width, height} = Dimensions.get('window');
const MIN_HEIGHT = 128;
export const MAX_HEIGHT = height / 2;
const styles = StyleSheet.create({
  container: {
    width,
    height: MAX_HEIGHT,
    justifyContent: 'flex-end',
  },
  picture: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
  },
  title: {
    color: 'white',
    textAlign: 'center',
    fontSize: 32,
    fontWeight: '500',
  },
  titleContainer: {
    maxHeight: MAX_HEIGHT * 0.61,
    justifyContent: 'center',
    flex: 1,
  },
  mainTitle: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    padding: 32,
    transform: [{translateY: 64}],
  },
  subtitle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Item;
