import React from 'react';
import {
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const defaultList = [
    {
        name: 'Thêm',
        icon: 'add-circle-outline',
        color: 'red',
        id: 1,
    },
    {
        name: 'Báo thức',
        icon: 'alarm-outline',
        color: 'green',
        id: 2,
    },
    {
        name: 'Albums',
        icon: 'albums-outline',
        color: 'blue',
        id: 3,
    },
    {
        name: 'Nén file',
        icon: 'archive-outline',
        color: 'black',
        id: 4,
    },
    {
        name: 'Biểu đồ',
        icon: 'bar-chart-outline',
        color: 'orange',
        id: 5,
    },
    {
        name: 'Shopping',
        icon: 'basket-outline',
        color: 'pink',
        id: 6,
    },
    {
        name: 'Pin',
        icon: 'battery-full-outline',
        color: 'yellow',
        id: 7,
    },
    {
        name: 'Beer',
        icon: 'beer-outline',
        color: 'cyan',
        id: 8,
    },
    {
        name: 'Bluetooth',
        icon: 'bluetooth-outline',
        color: 'green',
        id: 9,
    },
    {
        name: 'Tool',
        icon: 'briefcase-outline',
        color: 'red',
        id: 10,
    },
];

const Item = (item, index) => {
    return (
        <TouchableOpacity
            key={index.toString()}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                width: 70,
                paddingVertical: 10,
                borderRadius: 5,
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.27,
                shadowRadius: 4.65,
                marginRight: 15,
                elevation: 6,
            }}>
            <Icon name={item.icon} color={item.color} size={30} />
            <Text
                style={{
                    color: '#555555',
                    fontSize: 14,
                    marginTop: 5,
                }}>
                {item.name}
            </Text>
        </TouchableOpacity>
    );
};

const ItemModule = (item, index) => {
    return (
        <TouchableOpacity
            key={index.toString()}
            style={{
                marginVertical: 10,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                paddingHorizontal: 15,
                paddingVertical: 10,
                borderRadius: 5,
                shadowColor: '#000',
                shadowOffset: {
                    width: 0,
                    height: 3,
                },
                shadowOpacity: 0.27,
                shadowRadius: 4.65,
                marginRight: 15,
                elevation: 6,
                flexDirection: 'row',
                marginLeft: 3
            }}>
            <Icon name={item.icon} color={item.color} size={20} />
            <Text
                style={{
                    color: '#555555',
                    fontSize: 14,
                    marginTop: 5,
                    lineHeight: 14,
                    marginLeft: 5
                }}>
                {item.name}
            </Text>
        </TouchableOpacity>
    );
};

export default Profile = props => {
    let list = [];
    props.route.params && props.route.params.selectedItem
        ? (list = props.route.params.selectedItem)
        : (list = defaultList.slice(0, 4));
    let fullList = [...defaultList];
    props.route.params && props.route.params.sortList
        ? (fullList = [...props.route.params.sortList])
        : null;
    return (
        <View style={styles.container}>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 15,
                    marginBottom: 25,
                }}>
                <Text
                    style={{
                        fontWeight: 'bold',
                        fontSize: 18,
                    }}>
                    Đề xuất
                </Text>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Drag', { list })}>
                    <Text style={{ color: 'blue' }}>Tùy chỉnh</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.list}>
                {list.map((item, index) => {
                    return Item(item, index);
                })}
            </View>

            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 15,
                    marginBottom: 25,
                    marginTop: 25,
                }}>
                <Text
                    style={{
                        fontWeight: 'bold',
                        fontSize: 18,
                    }}>
                    Danh sách module
                </Text>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('Sort', { fullList })}>
                    <Text style={{ color: 'blue' }}>Chỉnh sửa</Text>
                </TouchableOpacity>
            </View>
            <View>
                <ScrollView
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={{ paddingHorizontal: 20 }}>
                    {fullList.map((item, index) => {
                        return ItemModule(item, index);
                    })}
                    <View style={{ width: 20 }}></View>
                </ScrollView>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 70,
    },
    list: {
        flexDirection: 'row',
        paddingHorizontal: 30,
    },
});
