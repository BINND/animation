import React, { useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native'
import Animated, { useAnimatedStyle, useSharedValue, withSpring, withTiming } from 'react-native-reanimated';
import Toast from 'react-native-tiny-toast';
import Icon from 'react-native-vector-icons/Ionicons';


const ItemSelected = ({ item, index, onLongPress, onPressOut, onDelete, data }) => {
    const scale = useSharedValue(1);
    const sas = useAnimatedStyle(() => {
        return {
            transform: [{ scale: scale.value }],
        };
    });
    useEffect(() => {
        scale.value = 0;
        scale.value = withTiming(1, { duration: 300 })
    }, [])
    return (
        <Animated.View style={[{}, sas]} key={`@${index.toString()}`}>
            <TouchableOpacity

                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: 'white',
                    width: 70,
                    paddingVertical: 10,
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: {
                        width: 0,
                        height: 3,
                    },
                    shadowOpacity: 0.27,
                    shadowRadius: 4.65,
                    elevation: 6,
                }}
                onLongPress={onLongPress}
                onPressOut={onPressOut}>
                <Icon name={item.icon} color={item.color} size={30} />
                <Text
                    style={{
                        color: '#555555',
                        fontSize: 14,
                        marginTop: 5,
                    }}>
                    {item.name}
                </Text>
                <TouchableOpacity
                    onPress={async () => {
                        scale.value = withTiming(0, { duration: 300 });
                        setTimeout(() => onDelete(), 300);
                    }}
                    style={{
                        position: 'absolute',
                        backgroundColor: '#999999',
                        borderRadius: 22,
                        justifyContent: 'center',
                        alignItems: 'center',
                        width: 22,
                        height: 22,
                        top: -6,
                        right: -8,
                    }}>
                    <Icon name={'close'} color={'white'} size={16} />
                </TouchableOpacity>
            </TouchableOpacity>
        </Animated.View>
    );
};

export default ItemSelected