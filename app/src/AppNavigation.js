import React from 'react';
import Home from './Home';
import Chat from './Chat';
import Notification from './Notification';
import Profile from './Profile';
import { View, Text, Platform, StyleSheet, Dimensions } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import BottomSheet from 'reanimated-bottom-sheet';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import LiquidSwipe from './liquid-element/liquid-swipe';
import { createStackNavigator } from '@react-navigation/stack';
import Reflectly from './Reflectly/Reflectly';
import Chanel from './Chanel/Chanel';
import Pcoffee from './Pcoffee/Pcoffee';
import OnBoard from './Full-UI-Fashion/auth/OnBoard';
import WelCome from './Full-UI-Fashion/auth/Welcome';
import Login from './Full-UI-Fashion/auth2/Login';
import Wheel from './Spin-Wheel/Wheel';
import Wheel2 from './Spin-Wheel/Wheel2';
import DragMenu from './DragMenu';
import SortMenu from './SortModule';

const TouchableOpacity = require('react-native').TouchableOpacity;
const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();

function MyTabs() {
  return (
    <Tab.Navigator tabBar={props => <MyTabBar {...props} />}>
      <Tab.Screen name="Bubble" component={Home} />
      <Tab.Screen name="Swipe" component={Chat} />
      <Tab.Screen name="Animation" component={Notification} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}

const Fashion = createStackNavigator();

const FashionStack = () => {
  return (
    <NavigationContainer>
      <Fashion.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Fashion.Screen name="OnBoard" component={OnBoard} />
        <Fashion.Screen name="Welcome" component={WelCome} />
        <Fashion.Screen name="Login" component={Login} />
      </Fashion.Navigator>
    </NavigationContainer>
  );
};

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen name="Tab" component={MyTabs} />
        <Stack.Screen name="Wheel" component={Wheel2} />
        <Stack.Screen name="Drag" component={DragMenu} />
        <Stack.Screen name="Sort" component={SortMenu} />
        <Stack.Screen name="LiquidSwipe" component={LiquidSwipe} />
        <Stack.Screen name="Reflectly" component={Reflectly} />
        <Stack.Screen name="Chanel" component={Chanel} />
        <Stack.Screen name="Pcoffee" component={Pcoffee} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const width = Dimensions.get('screen').width;

export default MyStack;

function MyTabBar({ state, descriptors, navigation }) {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  const sheetRef = React.useRef(null);
  const animationValue = useSharedValue(0);
  const node = new Animated.Value(0);

  const animatedStyles = useAnimatedStyle(() => {
    return {
      transform: [
        { translateX: withSpring(animationValue.value, { damping: 100 }) },
      ],
    };
  });

  const animatedStyles1 = useAnimatedStyle(() => {
    return {
      transform: [
        { translateX: withSpring(animationValue.value - width, { damping: 100 }) },
      ],
    };
  });

  const bottom = (
    <View style={[{ flexDirection: 'row', width: '100%' }]}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
              ? options.title
              : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name);
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index.toString()}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={[styles.container]}
            enabledContentTapInteraction={true}>
            <Text style={{ color: isFocused ? '#673ab7' : '#222' }}>{label}</Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
  return (
    <BottomSheet
      enabledContentTapInteraction={false}
      ref={sheetRef}
      onCloseEnd={() => {
        animationValue.value = 0;
      }}
      onOpenEnd={() => {
        animationValue.value = width;
      }}
      snapPoints={[70, 450, 70]}
      contentPosition={node}
      renderContent={() => {
        return (
          <View
            style={{
              backgroundColor: 'white',
              padding: 16,
              height: 450,
            }}>
            <Animated.View style={animatedStyles}>{bottom}</Animated.View>
            <Animated.View
              style={[
                animatedStyles1,
                {
                  width: width,
                  height: 60,
                  position: 'absolute',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderBottomWidth: 1,
                  borderColor: '#E7E7E7',
                },
              ]}>
              <Text
                style={{
                  fontWeight: 'bold',
                  fontSize: 18,
                }}>
                Header
              </Text>
            </Animated.View>
          </View>
        );
      }}
    />
  );
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 15,
    backgroundColor: 'white',
    flex: 1,
  },
});
