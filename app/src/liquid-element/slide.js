import React from 'react';
import {Dimensions, Image, View, StyleSheet, Text} from 'react-native';

const {height, width} = Dimensions.get('window');

const Slide = props => {
  const slide = props.slide;
  return (
    <>
      <View style={[styles.container, {backgroundColor: slide.color}]}>
        <Image source={slide.picture} style={styles.image} />
        <View>
          <Text style={styles.title}>{slide.title}</Text>
          <Text style={styles.description}>{slide.description}</Text>
        </View>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    padding: 75,
    paddingTop: 150,
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 200,
  },
  title: {
    fontSize: 48,
    color: 'white',
    textAlign: 'center',
    marginBottom: 16,
  },
  description: {
    fontSize: 18,
    color: 'white',
    textAlign: 'center',
  },
});

export default Slide;
