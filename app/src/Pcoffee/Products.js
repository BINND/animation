import React from 'react';
import {View, StyleSheet, Image, Dimensions} from 'react-native';
import Animated, {interpolate, useAnimatedStyle} from 'react-native-reanimated';

const {width} = Dimensions.get('window');
const SIZE = 200;
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 60,
  },
});

const Products = ({x}) => {
  return (
    <View style={styles.container} pointerEvents="none">
      {products.map((product, index) => {
        const animated = useAnimatedStyle(() => {
          const translateX = interpolate(
            x.value,
            [width * (index - 1), width * index, width * (index + 1)],
            [width / 2, 0, -width / 2],
          );
          const scale = interpolate(
            x.value,
            [width * (index - 1), width * index, width * (index + 1)],
            [0.5, 1, 0.5],
          );
          return {transform: [{translateX}, {scale}]};
        });
        return (
          <Animated.View key={index} style={[styles.container, animated]}>
            <Image
              source={product.picture}
              style={{width: SIZE, height: SIZE * product.aspectRatio}}
            />
          </Animated.View>
        );
      })}
    </View>
  );
};

export default Products;

export const products = [
  {
    title: 'How about an Iced Coffee Rosé?',
    subtitle: 'Medium, Creamy Cream, Sweet Sugar, Iced',
    color1: '#F9AC8A',
    color2: '#FBC6AE',
    picture: require('../../assets/Pcoffee/rose.png'),
    aspectRatio: 1,
  },

  {
    title: 'Craving a Philharmonic?',
    subtitle: 'Large, Medium Cream, Medium Sugar',
    color1: '#4DD2A5',
    color2: '#63D8B0',
    picture: require('../../assets/Pcoffee/philharmonic.png'),
    aspectRatio: 1,
  },
  {
    title: 'Craving a new Cold Brew?',
    subtitle: 'Try Philtered Soul',
    color1: '#FEB829',
    color2: '#FDD446',
    picture: require('../../assets/Pcoffee/coldbrew.png'),
    aspectRatio: 1,
  },
  {
    title: 'Excited for an Ecstatic?',
    subtitle: 'Large, No cream, No sugar, Iced',
    color1: '#FE8E01',
    color2: '#FF9A16',
    picture: require('../../assets/Pcoffee/dark.png'),
    aspectRatio: 1,
  },
  {
    title: 'Craving a Croissant?',
    subtitle: 'Flaky perfection, baked fresh daily',
    color1: '#E2DDD1',
    color2: '#F3F1ED',
    picture: require('../../assets/Pcoffee/croissant.png'),
    aspectRatio: 757 / 735,
  },
];
