import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Card, {CARD_HEIGHT} from './Card';
import Icon from 'react-native-vector-icons/Feather';
import Cards from './Cards';
import Products from './Products';
import Animated, {
  interpolate,
  interpolateColor,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';
import {interpolatePath} from 'react-native-redash';

const {width} = Dimensions.get('window');

const snapToOffsets = [0, CARD_HEIGHT];

const Pcoffee = props => {
  const translateX = useSharedValue(0);
  const onScroll = useAnimatedScrollHandler({
    onScroll: ({contentOffset: {x}}) => {
      translateX.value = x;
    },
  });
  const style = useAnimatedStyle(() => ({
    backgroundColor: interpolateColor(
      translateX.value,
      products.map((_, i) => width * i),
      products.map(product => product.color2),
    ),
  }));
  return (
    <Animated.View style={[styles.container, style]}>
      <ScrollView
        snapToOffsets={snapToOffsets}
        decelerationRate="fast"
        bounces={false}
        snapToEnd={false}
        showsVerticalScrollIndicator={false}>
        <View style={styles.slider}>
          <Animated.ScrollView
            onScroll={onScroll}
            scrollEventThrottle={16}
            decelerationRate="fast"
            snapToInterval={width}
            horizontal
            showsHorizontalScrollIndicator={false}>
            {products.map((product, index) => (
              <Card product={product} key={index} />
            ))}
          </Animated.ScrollView>
          <Products x={translateX} />
        </View>
        <Cards />
      </ScrollView>
      <TouchableOpacity
        onPress={() => props.navigation.goBack()}
        style={styles.back}>
        <Icon name="arrow-left" size={30} color={'black'} />
      </TouchableOpacity>
    </Animated.View>
  );
};

export default Pcoffee;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
  },
  slider: {
    height: CARD_HEIGHT,
  },
  back: {
    position: 'absolute',
    top: 30,
    left: 10,
  },
});
export const products = [
  {
    title: 'How about an Iced Coffee Rosé?',
    subtitle: 'Medium, Creamy Cream, Sweet Sugar, Iced',
    color1: '#F9AC8A',
    color2: '#FBC6AE',
    picture: require('../../assets/Pcoffee/rose.png'),
    aspectRatio: 1,
  },

  {
    title: 'Craving a Philharmonic?',
    subtitle: 'Large, Medium Cream, Medium Sugar',
    color1: '#4DD2A5',
    color2: '#63D8B0',
    picture: require('../../assets/Pcoffee/philharmonic.png'),
    aspectRatio: 1,
  },
  {
    title: 'Craving a new Cold Brew?',
    subtitle: 'Try Philtered Soul',
    color1: '#FEB829',
    color2: '#FDD446',
    picture: require('../../assets/Pcoffee/coldbrew.png'),
    aspectRatio: 1,
  },
  {
    title: 'Excited for an Ecstatic?',
    subtitle: 'Large, No cream, No sugar, Iced',
    color1: '#FE8E01',
    color2: '#FF9A16',
    picture: require('../../assets/Pcoffee/dark.png'),
    aspectRatio: 1,
  },
  {
    title: 'Craving a Croissant?',
    subtitle: 'Flaky perfection, baked fresh daily',
    color1: '#E2DDD1',
    color2: '#F3F1ED',
    picture: require('../../assets/Pcoffee/croissant.png'),
    aspectRatio: 757 / 735,
  },
];
