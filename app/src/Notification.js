import React, {Component, useState} from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';

const Notification = props => {
  const animationList = [
    {
      lable: 'Liquid Swipe',
      onPress: () => {
        props.navigation.navigate('LiquidSwipe');
      },
    },
    {
      lable: 'Reflectly',
      onPress: () => {
        props.navigation.navigate('Reflectly');
      },
    },
    {
      lable: 'Chanel',
      onPress: () => {
        props.navigation.navigate('Chanel');
      },
    },
    {
      lable: 'Pcoffee',
      onPress: () => {
        props.navigation.navigate('Pcoffee');
      },
    },
    {
      lable: 'Fashion UI',
      onPress: () => {
        props.navigation.navigate('Fashion');
      },
    },
  ];

  renderItem = ({item, index}) => {
    return (
      <TouchableOpacity style={styles.button} onPress={item.onPress}>
        <Text>{item.lable}</Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={animationList}
        keyExtractor={(item, index) => index}
        renderItem={renderItem}
      />
    </View>
  );
};

export default Notification;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 60,
  },
  button: {
    backgroundColor: 'white',
    height: 50,
    width: '80%',
    marginHorizontal: '10%',
    borderRadius: 10,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#7A7A7A',
    marginBottom: 15,
  },
});
