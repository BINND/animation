import React, { Component, useRef, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Dimensions,
    TouchableWithoutFeedback,
} from 'react-native';
import Animated, {
    useSharedValue,
    useAnimatedStyle,
    withSpring,
    withTiming,
    withDecay,
    withDelay,
    withRepeat,
    withSequence,
    useAnimatedGestureHandler,
    runOnJS,
} from 'react-native-reanimated';
import {
    TapGestureHandler,
    PanGestureHandler,
} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';

Home = () => {
    const { height, width } = Dimensions.get('window');
    const [positon, setPositon] = useState({});
    const [press, setPress] = useState(true);
    const startingPosition = 100;
    const x = useSharedValue(width - 80);
    const y = useSharedValue(100);
    const scale = useSharedValue(0);
    const endX = useSharedValue(width - 80);
    const endY = useSharedValue(100);
    const cancel = useSharedValue(200);

    const cancelStyle = useAnimatedStyle(() => {
        return {
            transform: [{ translateY: cancel.value }],
        };
    });

    const eventHandler = useAnimatedGestureHandler({
        onStart: (event, ctx) => {
            ctx.startX = x.value;
            ctx.startY = y.value;
            cancel.value = withSpring(0);
        },
        onActive: (event, ctx) => {
            x.value = withSpring(ctx.startX + event.translationX);
            if (
                ctx.startY + event.translationY < height - 100 &&
                ctx.startY + event.translationY > 50
            ) {
                y.value = withSpring(ctx.startY + event.translationY);
            }
        },
        onEnd: (event, ctx) => {
            cancel.value = withSpring(200);
            endX.value = withSpring(ctx.startX + event.translationX);
            endY.value = withSpring(ctx.startY + event.translationY);
        },
    });

    const handler = useAnimatedGestureHandler({
        onStart: (event, ctx) => {

        },
        onActive: (event, ctx) => {

        },
        onEnd: (event, ctx) => {

        },
    });

    const uas = useAnimatedStyle(() => {
        return {
            //backgroundColor: pressed.value ? '#FEEF86' : '#001972',
            transform: [{ translateX: x.value }, { translateY: y.value }],
        };
    });

    const sas = useAnimatedStyle(() => {
        return {
            transform: [{ scale: scale.value }],
        };
    });

    const onCancel = () => {
        if (scale.value == 1) {
            x.value = withTiming(endX.value, {
                duration: 200,
            });
            y.value = withTiming(endY.value, {
                duration: 200,
            });
            scale.value = withTiming(0);
            setPress(true);
        }
    };

    const cancelView = useRef();
    return (
        <TouchableWithoutFeedback onPress={onCancel}>
            <View style={styles.container}>
                <PanGestureHandler onGestureEvent={eventHandler} enabled={press}>
                    <Animated.View
                        style={[
                            {
                                borderRadius: 60,
                                height: 60,
                                width: 60,
                                backgroundColor: 'rgba(0, 0,0,0.7)',
                                justifyContent: 'center',
                                alignItems: 'center',
                                zIndex: 10,
                            },
                            uas,
                        ]}>
                        <View
                            style={{
                                borderRadius: 45,
                                height: 45,
                                width: 45,
                                backgroundColor: '#888888',
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderWidth: 1,
                                borderColor: '#777777',
                            }}>
                            <View
                                style={{
                                    borderRadius: 37,
                                    height: 37,
                                    width: 37,
                                    backgroundColor: '#999999',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderWidth: 1,
                                    borderColor: '#777777',
                                }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        x.value = withTiming(width / 2 - 30, {
                                            duration: 200,
                                        });
                                        y.value = withTiming(height - 200, {
                                            duration: 200,
                                        });
                                        scale.value = withTiming(1);
                                        setPress(false);
                                    }}
                                    style={{
                                        borderRadius: 30,
                                        height: 30,
                                        width: 30,
                                        backgroundColor: 'white',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderWidth: 1,
                                        borderColor: '#777777',
                                    }}></TouchableOpacity>
                            </View>
                        </View>
                    </Animated.View>
                </PanGestureHandler>
                <TouchableWithoutFeedback onPress={() => null}>
                    <Animated.View style={[styles.menu, sas]}>
                        <View style={styles.left}>
                            <View style={styles.unit}>
                                <Icon
                                    name="star"
                                    color="white"
                                    size={50}
                                />
                                <Text style={styles.title}>Tùy chỉnh</Text>
                            </View>
                            <View style={styles.unit}>
                                <Icon
                                    name="volume-high"
                                    color="white"
                                    size={50}
                                />
                                <Text style={styles.title}>Âm lượng</Text>
                            </View>
                        </View>
                        <View style={styles.middle}>
                            <Icon
                                name="notifications-circle"
                                color="white"
                                size={50}
                            />
                            <Text style={styles.title}>Thông báo</Text>
                        </View>
                        <View style={styles.right}>
                            <View style={styles.unit}>
                                <Icon
                                    name="ios-mic-circle"
                                    color="white"
                                    size={50}
                                />
                                <Text style={styles.title}>Micro</Text>
                            </View>
                            <View style={styles.unit}>
                                <Icon
                                    name="ios-phone-portrait-sharp"
                                    color="white"
                                    size={50}
                                />
                                <Text style={styles.title}>Thiết bị</Text>
                            </View>
                        </View>
                    </Animated.View>
                </TouchableWithoutFeedback>
            </View>
        </TouchableWithoutFeedback>
    );
};

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingBottom: 100,
    },
    menu: {
        backgroundColor: 'rgba(0, 0,0,0.7)',
        width: 300,
        height: 280,
        borderRadius: 10,
        alignSelf: 'center',
        position: 'absolute',
        bottom: 100,
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 15
    },
    unit: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    title: { color: 'white', marginTop: 10 },
    middle: { alignItems: 'center' }
});
